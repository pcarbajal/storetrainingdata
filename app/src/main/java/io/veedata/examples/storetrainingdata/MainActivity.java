package io.veedata.examples.storetrainingdata;

import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.NetworkOnMainThreadException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button sendButton = (Button) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        //convert parameters into JSON object
        Map<String, String> params = new HashMap<>();
        params.put("Hello", "World");
        try {
            JSONObject data = getJsonObjectFromMap(params);
            new SendDataTask().execute(data);

        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }

    }

    private JSONObject getJsonObjectFromMap(Map params) throws JSONException {

        //all the passed parameters from the post request
        //iterator used to loop through all the parameters
        //passed in the post request
        Iterator iter = params.entrySet().iterator();

        //Stores JSON
        JSONObject holder = new JSONObject();

        //using the earlier example your first entry would get email
        //and the inner while would get the value which would be 'foo@bar.com'
        //{ fan: { email : 'foo@bar.com' } }

        //While there is another entry
        while (iter.hasNext())
        {
            //gets an entry in the params
            Map.Entry pairs = (Map.Entry)iter.next();

            //creates a key for Map
            String key = (String)pairs.getKey();

            //Create a new map
            String data = (String)pairs.getValue();

            //puts email and 'foo@bar.com'  together in map
            holder.put(key, data);
        }
        return holder;
    }

    private class SendDataTask extends AsyncTask<JSONObject, Integer, Void> {
        // Do the long-running work in here
        protected Void doInBackground(JSONObject... data) {
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) new URL("http://veedata-1007.appspot.com/getMoving").openConnection();
                connection.setDoOutput(true);
                connection.setDoInput(true);
//                connection.setRequestProperty("Content-Type", "application/json");
//                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestMethod("POST");
                connection.connect();

                //Write
                String str =  "Hello world!";
                byte[] outputInBytes = str.getBytes("UTF-8");
                OutputStream os = connection.getOutputStream();
                os.write( outputInBytes );
                os.close();

//                OutputStream os = connection.getOutputStream();
//                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
//                writer.write(data[0].toString());
//                writer.close();
//                os.close();

//                connection = (HttpURLConnection) new URL("http://www.android.com/").openConnection();
//                connection.connect();
//                InputStream in = new BufferedInputStream(connection.getInputStream());
//
//                BufferedReader r = new BufferedReader(new InputStreamReader(in));
//                StringBuilder total = new StringBuilder();
//                String line;
//                while ((line = r.readLine()) != null) {
//                    total.append(line);
//                }
//
//                Log.i(TAG, total.toString());

            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);

            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }

            return null;
        }

        // This is called when doInBackground() is finished
        protected void onPostExecute(Void result) {
        }

    }
}
